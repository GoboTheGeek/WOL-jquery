package ch.gobothegeek.tech.wol.model.json;

// a json wrapper used to receive a WOL request
public class WolRequest {
	private String computer;
	private String mac;

	public WolRequest() { }

	public WolRequest(String computer, String mac) {
		this.computer = computer;
		this.mac = mac;
	}

	public String getComputer() { return computer; }
	public void setComputer(String computer) { this.computer = computer; }

	public String getMac() { return mac; }
	public void setMac(String mac) { this.mac = mac; }
}

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/