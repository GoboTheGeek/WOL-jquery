package ch.gobothegeek.tech.wol.model.json;

// a json wrapper used respond to WOL query
public class WolResponse {
	private String computer;
	private Boolean wakeSent;

	public WolResponse() { }

	public WolResponse(String computer, Boolean wakeSent) {
		this.computer = computer;
		this.wakeSent = wakeSent;
	}

	public String getComputer() { return computer; }
	public void setComputer(String computer) { this.computer = computer; }

	public Boolean getWakeSent() { return wakeSent; }
	public void setWakeSent(Boolean wakeSent) { this.wakeSent = wakeSent; }
}

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/