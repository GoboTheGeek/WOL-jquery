// This class display a static list of wakeable computers.
// computers are configured in "configuration.js" file.
class ComputerUI extends Computer {
    constructor(name, dns, user, mac, messages) {
        super(name, dns, user, mac);
        this.messages = messages;
        this.clickID = 'wol-computer-' + this.computerName;
        this.cssColor = { on: null, off: 'btn-light disabled' }; // css class to apply to button
        this.cssIcon = { on: 'fa-computer', off: 'fa-circle-xmark' }; // css class to apply to button's icon
    };

    // return HTML to display informations.
    display(even) {
        let html = '';

        this.cssColor.on = (even? 'btn-outline-primary' : 'btn-outline-info');
        html += '<div class="col col-md-4 wol-computer">';
        html += '<a id="' + this.clickID + '" class="btn btn-sm">';
        html += '<i class="fa-solid float-start"></i>';
        html += this.userName;
        html += '</a>';
        html += '</div>';
        return html;
    };

    // register click event
    registerClick() {
        $('#' + this.clickID).off('click');
        $('#' + this.clickID).on('click', this.startWol.bind(this));
    };

    // show or hide button
    changeVisibility(visible) {
        if (visible) {
            $('#' + this.clickID).removeClass(this.cssColor.off).addClass(this.cssColor.on);
            $('#' + this.clickID).find('i').removeClass(this.cssIcon.off).addClass(this.cssIcon.on);
        } else {
            $('#' + this.clickID).removeClass(this.cssColor.on).addClass(this.cssColor.off);
            $('#' + this.clickID).find('i').removeClass(this.cssIcon.on).addClass(this.cssIcon.off);
        }
    };

    // launch wake on lan process
    startWol() {
        this.messages.showWait();
        this.requireWOL();
    };

    // called when WOL request has failed (http/4xx or http/5xx error)
    requestFailed(error) {
        this.messages.hideWait();
        this.messages.showError();
    };

    // called when WOL request is successfull (http/200)
    requestOk(data) {
        this.messages.hideWait();
        this.messages.showOk();
    };
};

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/