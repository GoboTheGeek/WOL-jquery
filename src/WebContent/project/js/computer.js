// this class allows a user to request a wake up of a specified computer
class Computer {
    constructor(name, dns, user, mac) {
        this.name = name; // computer name (for statistics)
        this.dns = dns; // dns name for computer
        this.user = user; // username of computer (so user can find easily its computer)
        this.mac = mac; // mac address of computer (only way to use WOL)
    };

    // ask server to produce a wake on lan request to wake up computer.
    requireWOL() {
        let that = this;

        fetch('wol/computer/wakeup', {
            method: 'POST',
            body: JSON.stringify( { computer: that.dns, mac: this.mac } ),
            headers: { "Content-Type": "application/json" }
        } ).
        then((response) => {
            if (200 === response.status) {
                return response.json();
            } else {
                throw response.status;
            }
        } ).
        then((json) => {
            that.requestOk(json);
        } ).
        catch((error) => {
            that.requestFailed(error);
        } );
    };

    // called when WOL request has failed (http/4xx or http/5xx error)
    requestFailed(error) { };
    // called when WOL request is successfull (http/200)
    requestOk(data) { };

    // getters
    get computerName() { return this.name; };
    get computerDnsName() { return this.dns; };
    get userName() { return this.user; };
    get macAddr() { return this.mac; };
};

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/