// This class display an wait overlay during wake up.
class MessagesUI {
    constructor() {
        $('#wol-error-close').click(this.hideError.bind(this));
    };

    showWait() { $('#wol-wait').show(); };
    hideWait() { $('#wol-wait').fadeOut(100, function() { $('#wol-wait').hide(); } ); };

    showOk() { $('#wol-ok').show(); };
    hideOk() { $('#wol-ok').fadeOut(100, function() { $('#wol-ok').hide(); } ); };

    showError() { $('#wol-error').show(); };
    hideError() { $('#wol-error').fadeOut(100, function() { $('#wol-error').hide(); } ); };
};

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/