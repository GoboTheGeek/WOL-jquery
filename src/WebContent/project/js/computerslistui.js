// use configuration to prepare list of instances of ComputerUI.
class ComputersListUI {
    constructor() {
        this.computers = []; // lust of all available computers
        this.messages = new MessagesUI();
    };

    // initialize list
    init() {
        for (let pos = 0; pos < CONFIG.computers.length; pos++) {
            this.computers.push(new ComputerUI(CONFIG.computers[pos].name, CONFIG.computers[pos].name + '.' + CONFIG.domain,
                                                CONFIG.computers[pos].user, CONFIG.computers[pos].mac, this.messages));
        };
        $('#wol-filter').keyup(this.filterDisplay.bind(this));
        $('#wol-clear').click(this.filterClear.bind(this));
        this.messages.hideWait();
        this.messages.hideOk();
        this.messages.hideError();
    };

    // display buttons for each computer
    display(divId) {
        let html = '';

        for (let pos = 0; pos < this.computers.length; pos++) {
            if (pos % 3 === 0) { html += (0 !== html.length? '</div>' : '') + '<div class="row wol-row">'; }
            html += this.computers[pos].display((0 === pos % 2));
        }
        html += (!html.endsWith('/<div>')? '</div>' : '');
        $(divId).html(html);
        for (let pos = 0; pos < this.computers.length; pos++) {
            this.computers[pos].registerClick();
        }
        this.filterClear();
    };

    // show buttons for username starting with filter
    filterDisplay() {
        for (let pos = 0; pos < this.computers.length; pos++) {
            this.computers[pos].changeVisibility(false);
        }
        for (let pos = 0; pos < this.computers.length; pos++) {
            if (this.computers[pos].userName.toLowerCase().startsWith($('#wol-filter').val())) { this.computers[pos].changeVisibility(true); }
        }
    };

    // clear filter
    filterClear() {
        $('#wol-filter').val('');
        this.filterDisplay();
    };
};

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-jquery".

"WOL-jquery" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-jquery" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-jquery".  If not, see <https://www.gnu.org/licenses/>.
*/