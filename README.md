# WOL-jquery


__Wake On LAN__: affiche une liste statique d'ordinateurs configurés pour le "wake on lan".

Cette version repose sur Bootstrap 5, Font awesome 6 et jQuery 3.6 pour le front end. De son côté, le back end repose sur Apache Deltaspike v1.9 et fonctionne sous Apache TomEE 8.

## English version

__Wake On LAN__: displays a static list of wakeable computers.

This version uses Bootstrap 5, Font awesome 6 and jQuery 3.6 for front end. Back end uses Apache Deltaspike 1.9 and TomEE 8.